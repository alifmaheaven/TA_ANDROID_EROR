package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.RecyclerView.RecyclerViewAdapter;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite.DataHelper;

public class MainActivity extends AppCompatActivity {


    public static MainActivity ma;

    protected Cursor cursor;
    String[] daftar;
    Menu menu;
    DataHelper MyDatabase;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList noiventarisList;
    private ArrayList namaasetList;
    private ArrayList hargasaruanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buat = findViewById(R.id.Buat);

        buat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent inte = new Intent(MainActivity.this, BuatInventaris.class);
                startActivity(inte);
                finish();
            }
        });

        noiventarisList = new ArrayList<>();
        namaasetList = new ArrayList<>();
        hargasaruanList = new ArrayList<>();
        ma = this;

        MyDatabase = new DataHelper(getBaseContext());
        recyclerView = findViewById(R.id.recycler);

        RefreshList();


        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapter = new RecyclerViewAdapter(noiventarisList, namaasetList, hargasaruanList);
        //Memasang Adapter pada RecyclerView
        recyclerView.setAdapter(adapter);
        //Membuat Underline pada Setiap Item Didalam List
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.line));
        recyclerView.addItemDecoration(itemDecoration);
    }


    public void RefreshList() {
        SQLiteDatabase db = MyDatabase.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM inventaris", null);

        cursor.moveToFirst();
        for (int count = 0; count < cursor.getCount(); count++) {

            cursor.moveToPosition(count);//Berpindah Posisi dari no index 0 hingga no index terakhir

            noiventarisList.add(cursor.getString(0));
            namaasetList.add(cursor.getString(1));
            hargasaruanList.add(cursor.getString(8));


        }


    }


    public void onItemClick(View view, int position) {
        Context context = view.getContext();
        Intent intent = new Intent();
        switch (position) {
            case 0:
                intent = new Intent(context, LihatInventaris.class);
                context.startActivity(intent);
                break;
        }
    }
}
