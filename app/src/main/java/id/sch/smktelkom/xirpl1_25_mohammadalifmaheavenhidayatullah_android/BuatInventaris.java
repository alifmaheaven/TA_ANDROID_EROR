package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite.DataHelper;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.SharedPrefManager;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.User;

@SuppressWarnings("ALL")
public class BuatInventaris extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;
    EditText namaaset, spesifikasi, tanggalpengadaan, namaruangan, idkategori, idtype, noaset, hargasatuan, satuan;
    Button simpan, kembali;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_inventaris);
        final User user = SharedPrefManager.getInstance(this).getUser();
        dbHelper = new DataHelper(this);


        namaaset = findViewById(R.id.NamaAset);
        spesifikasi = findViewById(R.id.Spesifikasi);
        tanggalpengadaan = findViewById(R.id.TanggalPengadaan);
        namaruangan = findViewById(R.id.NamaRuangan);
        idkategori = findViewById(R.id.IDKategori);
        idtype = findViewById(R.id.IDType);
        noaset = findViewById(R.id.NoAset);
        hargasatuan = findViewById(R.id.HargaSatuan);
        satuan = findViewById(R.id.Satuan);

        simpan = findViewById(R.id.Simpan);
        kembali = findViewById(R.id.Kembali);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                namaaset.getText();
                spesifikasi.getText();
                tanggalpengadaan.getText();
                namaruangan.getText();
                idkategori.getText();
                Integer.valueOf(idtype.getText().toString());
                Integer.valueOf(noaset.getText().toString());
                Integer.valueOf(hargasatuan.getText().toString());
                satuan.getText();

                String noinventaris = user.getBranch();

                // TODO Auto-generated method stub
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("insert into inventaris(NoInventaris, NamaAset, Spesifikasi, TanggalPengadaan, NamaRuangan, IDKategori, IDType, NoAset, HargaSatuan, Satuan) values ('" +
                        "malang" + "','" +
                        namaaset + "','" +
                        spesifikasi + "','" +
                        tanggalpengadaan + "','" +
                        namaruangan + "','" +
                        idkategori + "','" +
                        idtype + "','" +
                        noaset + "','" +
                        hargasatuan + "','" +
                        satuan + "')");
                Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
                MainActivity.ma.RefreshList();
                startActivity(new Intent(BuatInventaris.this, MainActivity.class));
                finish();
            }
        });

    }
}
