package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite.DataHelper;

public class UpdateInventaris extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    EditText namaaset, spesifikasi, tanggalpengadaan, namaruangan, idkategori, idtype, noaset, hargasatuan, satuan;
    Button simpan, kembali;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_inventaris);

        namaaset = findViewById(R.id.NamaAset);
        spesifikasi = findViewById(R.id.Spesifikasi);
        tanggalpengadaan = findViewById(R.id.TanggalPengadaan);
        namaruangan = findViewById(R.id.NamaRuangan);
        idkategori = findViewById(R.id.IDKategori);
        idtype = findViewById(R.id.IDType);
        noaset = findViewById(R.id.NoAset);
        hargasatuan = findViewById(R.id.HargaSatuan);
        satuan = findViewById(R.id.Satuan);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        cursor = db.rawQuery("SELECT * FROM inventaris WHERE NoInvetaris = '" +
                getIntent().getStringExtra("no") + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            namaaset.setText(cursor.getString(2).toString());
            spesifikasi.setText(cursor.getString(3).toString());
            tanggalpengadaan.setText(cursor.getString(4).toString());
            namaruangan.setText(cursor.getString(5).toString());
            idtype.setText(cursor.getString(6).toString());
            noaset.setText(cursor.getString(7).toString());
            hargasatuan.setText(cursor.getString(8).toString());
            satuan.setText(cursor.getString(9).toString());

        }





    }
}
