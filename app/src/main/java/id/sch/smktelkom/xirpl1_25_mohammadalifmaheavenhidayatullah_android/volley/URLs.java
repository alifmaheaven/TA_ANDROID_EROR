package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley;

public class URLs {
    private static final String ROOT_URL = "http://10.1.5.25:8080/users";

    public static final String URL_REGISTER = ROOT_URL + "signup";
    public static final String URL_LOGIN = ROOT_URL;
}
