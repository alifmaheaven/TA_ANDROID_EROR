package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.LihatInventaris;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList noinventarisList; //Digunakan untuk Nama
    private ArrayList namaasetList; //Digunakan untuk Jurusan
    private ArrayList hargasatuanList; //Digunakan untuk Jurusan

    //Membuat Konstruktor pada Class RecyclerViewAdapter


    public RecyclerViewAdapter(ArrayList noinventarisList, ArrayList namaasetList, ArrayList hargasatuanList) {
        this.noinventarisList = noinventarisList;
        this.namaasetList = namaasetList;
        this.hargasatuanList = hargasatuanList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Membuat View untuk Menyiapkan dan Memasang Layout yang Akan digunakan pada RecyclerView
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_design, parent, false);
        ViewHolder viewHolder = new ViewHolder(V);
        return new ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        //Memanggil Nilai/Value Pada View-View Yang Telah Dibuat pada Posisi Tertentu
        final String noinventaris = (String) noinventarisList.get(position);//Mengambil data (Nama) sesuai dengan posisi yang telah ditentukan
        final String namaaset = (String) namaasetList.get(position);//Mengambil data (Jurusan) sesuai dengan posisi yang telah ditentukan
        final String hargasatuan = (String) hargasatuanList.get(position);//Mengambil data (NIM) sesuai dengan posisi yang telah ditentukan


        holder.noinventaris.setText(noinventaris);
        holder.namaaset.setText(namaaset);
        holder.hargasatuan.setText(hargasatuan);

        holder.item.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                String noin = holder.noinventaris.getText().toString();

                Intent i = new Intent(view.getContext(), LihatInventaris.class);

                i.putExtra("no", noin);

                view.getContext().startActivity(i);

            }
        });


    }


    @Override
    public int getItemCount() {
        //Menghitung Ukuran/Jumlah Data Yang Akan Ditampilkan Pada RecyclerView
        return hargasatuanList.size();
    }


    //ViewHolder Digunakan Untuk Menyimpan Referensi Dari View-View
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView noinventaris, namaaset, hargasatuan;
        private RelativeLayout item;
        private ImageView img;

        ViewHolder(View itemView) {
            super(itemView);
            //Menginisialisasi View-View untuk kita gunakan pada RecyclerView
            noinventaris = itemView.findViewById(R.id.NoInventaris);
            namaaset = itemView.findViewById(R.id.NamaAset);
            hargasatuan = itemView.findViewById(R.id.HargaSatuan);
            item = itemView.findViewById(R.id.item_list);
        }
    }

}
