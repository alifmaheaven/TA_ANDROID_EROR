package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "inventaris.db";
    private static final int DATABASE_VERSION = 1;

    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String sql = "create table inventaris(NoInventaris varchar(100) primary key, NamaAset varchar(50) null, Spesifikasi text null, TanggalPengadaan date null, NamaRuangan varchar(50) null, IDKategori varchar(11) null, IDType int(11) null,NoAset int null,HargaSatuan int(50) null, Satuan varchar(50) null);";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);
        sql = "INSERT INTO inventaris(NoInventaris, NamaAset, Spesifikasi, TanggalPengadaan, NamaRuangan, IDKategori, IDType, NoAset, HargaSatuan, Satuan) VALUES ('lcd89098943894','Motherboard','guguguguguguguug',2018-05-09,'Ruang melati','A',10,50,850000,'satuset');";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub
    }


}
